pipeline {
    agent any
    tools {nodejs "node"}
    stages {
        stage('Install unity') {
            steps {
                sh 'sh ./despesa_scripst/install.sh' 
                cleanWs()
            }
        }
        stage('Teste Package') {
            steps {
                sh 'npm install'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
