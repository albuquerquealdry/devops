import os
from dotenv import load_dotenv


load_dotenv()

urlRepositoryHost = os.getenv('HOST-REP')#https://gitlab.com ou https://github.com
repositoryProvider = os.getenv('DIRECTORY-REP')
repositoryName =  "testes"

os.system(f" git clone {urlRepositoryHost}/{repositoryProvider}/{repositoryName}")
os.system(f" cp files/.gitlab-ci.yml tmp/{repositoryName}")
os.system(f"""cd tmp/{repositoryName} && git add . && git commit -m "ci-file add" && git push """)
